Hyrule Rebalance v7.5 PT-BR

Nome dos Itens -> Traduções

Hylian Shroom           -> Cogumelo Hylian
Hylian Rice             -> Arroz Hyrian
Hyrule Herb             -> Erva Hyrule
Bokoblin Guts           -> Víceras de Bokoblin
Moblin Guts             -> Víceras de Moblin
Moblin Horn             -> Chifre de Moblin
Lizalfos Tail           -> Cauda de Lizalfo
Icy Lizalfos Tail       -> Cauda de Lizalfo de Gelo
Red Lizalfos Tail       -> Cauda de Lizalfo de Fogo
Yellow Lizalfos Tail    -> Cauda de Lizalfo Elétrico
Farosh's Claw           -> Garra de Farosh
Shard of Farosh's Horn  -> Fragmento do Chifre de Farosh
Naydra's Claw           -> Garra de Naydra
Naydra's Scale          -> Escama de Naydra
Shard of Naydra's Fang  -> Fragmento da Presa de Naydra
Shard of Naydra's Horn  -> Fragmento do Chifre de Naydra
Dinraal's Claw          -> Garra de Dinraal
Shard of Dinraal's Horn -> Fragmento do Chifre de Dinraal
Shard of Dinraal's Fang -> Fragmento da Presa de Dinraal
Dinraal's Scale         -> Escama de Dinraal
Mighty Carp             -> Carpa Poderosa
Armored Carp            -> Carpa Blindada
Mighty Porgy            -> Salema Poderosa
Armored Porgy           -> Salema Blindada
Hyrule Bass             -> Truta Hyrule
Big Bass                -> Truta Grande
Staminoka Bass          -> Truta Staminoka
Chillfin Trout          -> Truta Gélida
Sizzlefin Trout         -> Truta Calorosa
Hebra Salmon            -> Salmão Hebra
Cool Safflina           -> Safflina Fresca
Electric Safflina       -> Safflina Elétrica
Spicy Pepper            -> Pimenta Picante
Ashen Lizard            -> Lagarto Cinzento
Sunset Firefly          -> Vagalume Pôr do Sol
Rushroom                -> Cogumelo Rápido
Ironshroom              -> Coguferro
Chillshroom             -> Cogumelo Frio
Sunshroom               -> Cogumelo Solar
Silent Shroom           -> Cogumelo Silencioso
Zapshroom               -> Cogumelo Certeiro
Razorshroom             -> Cogumelo Navalha
Luminous Stone          -> Pedra Luminosa
Ancient Screw           -> Parafuso Antigo
Fairy                   -> Fada
Wood                    -> Madeira
Maçã                    -> Maçã
Acorn                   -> Bolota
Flint                   -> Sílex
Chuchu Jelly            -> Geléia de Chuchu
White Chuchu Jelly      -> Geléia de Chuchu Branco
Red Chuchu Jelly        -> Geléia de Chuchu Vermelho
Yellow Chuchu Jelly     -> Geléia de Chuchu Amarela
Smotherwing Butterfly   -> Borboleta Sufocante
Winterwing Butterfly    -> Borboleta Asas de Inverno
Sneaky River Snail      -> Caracol Furtivo de Rio
Blueshell Snail         -> Caracol Casca Azul
Wildberry               -> Frutos Silvestres
Palm Fruit              -> Fruto da Palmeira
Raw Gourmet Meat        -> Carne Crua Gourmet
Keese Wing              -> Asa de Keese
Ice Keese Wing          -> Asa de Keese de Gelo
Electric Keese Wing     -> Asa de Keese Elétrico
Fire Keese Wing         -> Asa de Keese de Fogo
Keese Eyeball           -> Olho de Keese
Cold Darner             -> Libélula Fria
Blue Nightshade         -> Sombra da Noite Azul
Silent Princess         -> Princesa Silenciosa
Sanke Carp              -> Carpa Selvagem
Stealthfin Trout        -> Truta Furtiva
Fleet-Lotus Seeds       -> Semente de Lótus-do-Rio
Hightail Lizard         -> Lagarto de Cauda Longa
Swift Violet            -> Violeta Rápida
Swift Carrot            -> Cenoura Rápida
Hot-Footed Frog         -> Sapo Pé-Quente
Bee Honey               -> Mel de Abelha
Fortified Pumpkin       -> Abóbora Fortificada
Armoranth               -> Amaranto
Ironshell Crab          -> Caranguejo Casca de Ferro
Razorclaw Crab          -> Caranguejo Garra-Navalha
Bright-Eyed Crab        -> Caranguejo Olhos Brilhantes
Bladed Rhino Beetle     -> Besouro Rinoceronte Afiado
Rugged Rhino Beetle     -> Besouro Rinoceronte Áspero
Energetic Rhino Beetle  -> Besouro Rinoceronte Energético
Ancient Gear            -> Engrenagem Antiga
Ancient Spring          -> Mola Antiga
Ancient Shaft           -> Eixo Antigo
Ancient Core            -> Núcleo Antigo
Giant Ancient Core      -> Núcleo Antigo Gigante
Voltfin Trout           -> Truta Barbatana Elétrica
Voltfruit               -> Fruta Elétrica
Mighty Thistle          -> Cardo Poderoso
Mighty Bananas          -> Bananas Poderosas
Monster Extract         -> Extrato de Monstro
Tabantha Wheat          -> Trigo Tabantha
Fresh Milk              -> Leite Fresco
Goron Spice             -> Tempero Goron
Bird Egg                -> Ovo de Pássaro
Restless Cricket        -> Grilo Inquieto
Goat Butter             -> Manteiga de Cabra
Raw Bird Drumstick      -> Coxinha De Pássaro Crua
Raw Bird Thigh          -> Coxa de Pássaro Crua
Raw Whole Bird          -> Pássaro Inteiro Cru
Chickaloo Tree Nut      -> Noz Chickaloo
Hinox Toenail           -> Unha de Hinox
Hinox Tooth             -> Dente de Hinox
Lynel Horn              -> Chifre de Lynel
Hinox Guts              -> Víceras de Hinox
Raw Meat                -> Carne Crua
Octorok Tentacle        -> Tentáculo de Octorok
Octo Balloon            -> Balão Octo
Raw Prime Meat          -> Carne Crua Gourmet
Rock Salt               -> Pedra de Sal
Star Fragment           -> Fragmento de Estrela
Snowy Truffle           -> Trufa de Neve
Pinewood Truffle        -> Trufa-Pinho
Hydromelon              -> Hidromelão
Floria Durian           -> Durião Floria
Molduga Fin             -> Barbatana de Molduga
Molduga Guts            -> Víceras de Molduga
Hearty Radish           -> Rabanete Saudável
Cane Sugar              -> Cana de Açúcar
Blue Lizard             -> Largato Azul


----------------------------- Traduzindo itens ---------------------------------
Ultima atualização em Armor_114_Upper_Name(ArmorUpper.msbt)

- Uniforme da Guarda Real
O honrado uniforme da Guarda Real de Hyrule.
Feito de materiais leves, é elegante e prático.

- Camisa Gerudo
Traje padräo de Gerudo, popular pelo seu
design aberto e respirável e pelas 
tradicionais marcas Gerudo que o adornam.

- Gibão Quente
Um pulôver quente normalmente usado pelo
povo da montanha. Seu tecido resistente e luvas
grossas säo ótimo para manter o calor do corpo.

- Camisa Velha
Uma camisa fina bastante velha. Está com as
costuras abrindo, mas é melhor do que nada.
As mangas também säo um pouco curtas.

- Túnica do Crepúsculo
Diz a lenda que esta armadura foi usada por um
herói que lutou contra monstros do crepúsculo,
alguns pelos de lobo ainda estäo presos...

- Túnica do Vento
Diz a lenda que esta armadura já foi acalentada
por um herói que navegou o Grande Mar. É dito
ter sido um presente da sua avó.

- Túnica do Tempo
Diz a lenda que esta armadura foi usada por um herói
viajante do tempo. Estranhamente, seu design
especial cabe em crianças e adultos.

- Colete de Mergulhador
Peça essencial para quem quer procurar tesouros no
mar das nuvens. Seu design funciona maravilhas na água,
fortalecendo a capacidade de natação.

- Armadura Fantasma de Ganon
Alguns acreditam que esta armadura sombria foi inspirada
no fantasma do Rei do Mal. É uma capa etérea que
abafa o ruído, como a folha do vazio.

- Camiseta de Lagosta
O Herói dos Ventos uma vez vestiu esta camisa
para ir a uma ilha pitoresca que ele chamava de lar.
Equipar <null><null>melhora Folhas Korok<null>ÿÿ.

- Camisa Formiga
Essa camsia faz você se sentir uma linda fada.
Você se sente tão leve que poderia flutuar a
qualquer momento. Yuuu-Pi!


- Túnica Escura
Kilton desenvolveu esta túnica escura através da
sua pesquisa sobre monstros. Uma réplica sombria da
roupa verde, uma vez usada por um herói lendário.

- Armadura Fantasma
Lendas falam sobre espectros blindados que
aterrorizavam até os mais corajosos. Esta
peça se parecem com a armadura que eles usavam.

 - Armadura Zora
Armadura tradicional feita por cada princesa de
Zora para seu futuro marido. Vestindo 
lhe dará a capacidade de <null><null>nadar em cachoeiras<null>ÿÿ.

Armadura feita por Gorons para Hylians 
visitarem a cidade de Goron.
É feita de rochas resistentes ao fogo.

 - Túnica da Natureza
Esta armadura foi aparentemente criada para um
herói que viaja as florestas, e curiosamente,
ela cabe perfeitamente em você.

Armadura de Gerudo para homens. Contém safira,
que aproveita o poder do gelo para fazer
o calor mais suportável.

 - Peitoral Furtivo
Armadura projetada para furtividade, herdada
da antiga tribo Sheikah. Seu tecido especial
suprime o som.

- Armadura de Bárbaro
Armadura dada por um antigo guerreiro da tribo
da regiäo de Faron. A pintura de guerra alimenta
o seu espírito de luta.

Camisa Gerudo

 - Túnica Plumada da Neve 36
Feita por artesäos de Rito, esta túnica 
foi forrada com penas de Rito para ajudar a 
reter o calor do corpo 

- Equipamento de Escalada 14
A antiga tecnologia deste equipamento faz de
você um escalador melhor. As luvas especiais
antiderrapantes ajudam você esacalar mais rápido.

- Camisa Radiante
Uma camiseta vendida no Clube Secreto de 
Gerudo. Brilha no escuro devido a um 
corante feito de pedras luminosas.

- Armadura de Soldado
Guardas Hylians elogiam esta armadura 
por sua excelente proteção. Feita de placas 
robustas de metal.

21
Esta armadura foi criada pela antiga tecnologia
Sheikah usando peças de Guardiões. Reduz o dano 
recebido de armas antigas.

- Armadura de Borracha
Esta armadura tem resistência elétrica graças
a um material - uma antiga maravilha chamada "borracha".
Essa tecnologia näo existe mais.

- Túnica do Campeão
Na antiga Hyrule, esta peça só era usada por 
alguém que ganhou o respeito da família real.
Equipar <null><null>revela a vida do inimigo<null>ÿÿ.